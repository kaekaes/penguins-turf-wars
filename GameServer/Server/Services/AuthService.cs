﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Server.Models;
using SharedLibrary;

namespace Server.Services; 

public class AuthService : IAuthService {
    private readonly Settings _settings;
    private readonly GameDBContext _context;

    public AuthService(Settings settings, GameDBContext context) {
        _settings = settings;
        _context = context;
    }

    public (bool success, string content) Register(string username, string password) {
        if (_context.users.Any(u => u.Username == username)) return (false, "Username already taken");

        var user = new User() { Username = username, PasswordHash = password };
        user.ProvideSaltAndHash();

        _context.Add(user);
        _context.SaveChanges();

        return (true, "");
    }

    public (bool success, string token) Login(string username, string password) {
        var user = _context.users.SingleOrDefault(u => u.Username == username);
        if (user == null) return (false, "Invalid Username");
        
        if(user.PasswordHash != AuthHelper.ComputeHash(password, user.Salt)) return (false, "Invalid password");

        return (true, GenJWTToken(AssembleClaimsIdentity(user)));
    }

    private ClaimsIdentity AssembleClaimsIdentity(User user) {
        var subject = new ClaimsIdentity(new[] {
            new Claim("id", user.Id.ToString())
        });
        return subject;
    }

    private string GenJWTToken(ClaimsIdentity subject) {
        var tokenHandler = new JwtSecurityTokenHandler();
        byte[] key = Encoding.ASCII.GetBytes(_settings.Bearer);
        var tokenDesc = new SecurityTokenDescriptor {
            Subject = subject,
            Expires = DateTime.Now.AddYears(10),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDesc);
        return tokenHandler.WriteToken(token);
    }
    
}

public interface IAuthService {
    (bool success, string content) Register(string username, string password);
    (bool success, string token) Login(string username, string password);
}

public static class AuthHelper {

    public static void ProvideSaltAndHash(this User user) {
        var salt = GenerateSalt();
        user.Salt = Convert.ToBase64String(salt);
        user.PasswordHash = ComputeHash(user.PasswordHash, user.Salt);
    }

    private static byte[] GenerateSalt() {
        var rng = RandomNumberGenerator.Create();
        var salt = new byte[24];
        rng.GetBytes(salt);
        return salt;
    }

    public static string ComputeHash(string password, string saltS) {
        var salt = Convert.FromBase64String(saltS);

        using var hashGen = new Rfc2898DeriveBytes(password, salt);
        hashGen.IterationCount = 10101;
        var bytes = hashGen.GetBytes(24);
        return Convert.ToBase64String(bytes);
    }
    
}