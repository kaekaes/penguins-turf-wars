﻿using Microsoft.AspNetCore.Mvc;
using SharedLibrary;
using SharedLibrary.Requests;

namespace Server.Services;


public interface IPlayerService {
}

public class PlayerService : IPlayerService {
    private readonly GameDBContext _context;

    public PlayerService(GameDBContext context) {
        _context = context;
    }

}

// public class MockPlayerService : IPlayerService {
//     public void Coso() {
//         Console.WriteLine("Coso desde Mock");
//     }
// }