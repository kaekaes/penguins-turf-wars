﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using SharedLibrary;
using SharedLibrary.Requests;
using SharedLibrary.Responses;

namespace Server.Controllers; 

/// <summary>
/// Controller that only can be access if has the Header with bearer key and controlls logins and registers of players
/// </summary>
[Authorize]
[ApiController]
[Route("[controller]")]
public class PlayerController : ControllerBase {
    
    private readonly IPlayerService playerService;
    private readonly GameDBContext _context;

    /// <summary>
    /// Constructor del playerController
    /// </summary>
    /// <param name="playerService"></param>
    /// <param name="context"></param>
    public PlayerController(IPlayerService playerService, GameDBContext context) {
        this.playerService = playerService;
        this._context = context;
    }

    /// <summary>
    /// Register endpoint for a player
    /// </summary>
    /// <param name="req">Request that has the user that will held this player</param>
    /// <returns>Player's new info</returns>
    [HttpPost("register")]
    public IActionResult Register(CreatePlayerRequest req) {
        //User siempre existe porque tenemos tag Authorized
        var userId = int.Parse(User.FindFirst("id")?.Value);
        var user = _context.users.First(u => u.Id == userId);
        
        var player = new Player() {
            Name = req.Name,
            Level = 1,
            Experience = 0,
            User = user
        };

        user.Player = player;
        
        var existplayer = _context.players.SingleOrDefault(u => u.Id == player.User.Id);
        
        if (existplayer != null) return BadRequest("This user already has a Player assigned");
        
        _context.Add(player);
        _context.Update(user);
        _context.SaveChanges();

        PlayerResponse resp = new PlayerResponse() {
            Experience = player.Experience,
            Id = player.Id,
            Level = player.Level,
            Name = player.Name,
            UserId = player.User.Id
        };
        return Ok(resp);
    }
    
    [HttpGet("get")]
    public IActionResult Get() {
        var userId = int.Parse(User.FindFirst("id")?.Value);
        var player = _context.players.First(p => p.Id == userId);

        if (player == null)
            return BadRequest("Error while getting player");

        PlayerResponse resp = new PlayerResponse() {
            Experience = player.Experience,
            Id = player.Id,
            Level = player.Level,
            Name = player.Name,
            UserId = player.User.Id
        };

        return Ok(resp);
    }
}