﻿namespace SharedLibrary.Responses; 

[Serializable]
public class PlayerResponse {
    public int Id;
    public string Name;
    public int Level;
    public int Experience;

    public int UserId;
}
