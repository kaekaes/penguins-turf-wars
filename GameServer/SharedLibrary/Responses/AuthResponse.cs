﻿namespace SharedLibrary.Responses; 

[Serializable]
public class AuthResponse {
    public string token;
}