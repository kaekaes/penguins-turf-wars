using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

/// <summary>
/// Manager que se encarga de llevar toda la partida
/// </summary>
public class GameManager : MonoBehaviour{

    public static GameManager instance;

    [HideInInspector] public HudManager hud;
    [HideInInspector] public FindGame findGame;
	[HideInInspector] public ManagerNetwork server;
    public PenguinControllerMulti yoMismo;

    [HideInInspector] public Transform tiendaActual;
    public Transform extrasParent;
    public Transform redNexus, blueNexus, redMiniBoss, blueMiniBoss, redBoss, blueBoss;
    public Camera mainCamera;
    private float tiempoParaEmpezarPartida = 20f;
    private float tiempoSpawnEnemigos = 20f;
    public bool startSpawning = false;
    public int maxTotalBarrier = 350;
    public float bloodForMinions = 5f;
    public float bloodForJungle = 5f;
    public Transform tiendaBlue, tiendaRed;
	public bool gameStarted = false;


	// Est en un Awake porque varios sitios requieren de usar estos componentes en el Start
    void Awake(){
        if(instance == null) instance = this; // Singleton
        TryGetComponent<HudManager>(out hud);
        hud.loadingScreen.TryGetComponent<FindGame>(out findGame);
    }

    public void SetMiTienda(bool blue) => tiendaActual = blue ? tiendaBlue : tiendaRed;

	public Transform GetNexus(bool imBlue) { return imBlue ? blueNexus : redNexus; }

	public NexusController GetNexusController(bool imBlue) { return GetNexus(imBlue).GetChild(1).GetChild(1).GetComponent<NexusController>(); }

	/// <summary>
	/// Esta funcin se llama cuando un minion muere y hace que incremente el tamao de la barrera de un lado
	/// </summary>
	/// <param name="imBlue">Color del minion muerto</param>
    public void OnMinionDie(bool imBlue) { GetNexusController(!imBlue).AddBlood(bloodForMinions); }
	
	/// <summary>
	/// Esta funcin se llama cuando un enemigo de la jungla muere y hace que incremente el tamao de la barrera de un lado
	/// </summary>
	/// <param name="imBlue">Color del jugador que ha matado al minion</param>
	public void OnJungleMinionDie(bool imBlue) => GetNexusController(imBlue).AddBlood(bloodForJungle); 

	/// <summary>
	/// Va a la clase findGame que s es en red y hace un ClientRPC a todos para que se les sincronize la pantalla de carga
	/// </summary>
	/// <param name="timeToStartTheGame"></param>
    public void StartGame(float timeToStartTheGame) => findGame.StartGame(timeToStartTheGame);
	
	/// <summary>
	/// Hace que empiecen a aparecer minions de los nexos
	/// </summary>
    public void StartSpawningEnemies() => StartCoroutine(SpawnEnemiesOnNexus());
	
	private void SpawnRedBosses() {
		redMiniBoss.GetComponent<BossSpawn>().Spawn(false);
		redBoss.GetComponent<BossSpawn>().Spawn(false);
	}
	private void SpawnBlueBosses() {
		blueMiniBoss.GetComponent<BossSpawn>().Spawn(true);
		blueBoss.GetComponent<BossSpawn>().Spawn(true);
	}
   
	/// <summary>
	/// Es una corrutina que se va haciendo cada X tiempo y hace que aparezcan los minions de los nexos
	/// </summary>
	/// <returns></returns>
	IEnumerator SpawnEnemiesOnNexus() {
        SpawnRedBosses();
        SpawnBlueBosses();
        while (true) {

			if(!startSpawning) {
				yield return new WaitForSeconds(tiempoParaEmpezarPartida);
                Debug.Log("He acabado los " + tiempoParaEmpezarPartida + " segundos");
                startSpawning = true;
			}

			redNexus.GetComponent<EnemySpawn>().Spawn(false);
			blueNexus.GetComponent<EnemySpawn>().Spawn(true);
			yield return new WaitForSeconds(tiempoSpawnEnemigos);
			Debug.Log("Siguiente oleada");
		}
    }
}


