using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using Mirror;

public class DisplaySO : MonoBehaviour , IPointerEnterHandler , IPointerExitHandler
{

    public SOHabilidad habilidad;    
    private GameObject textHover;

	public bool vida, mana;

	List<Image> levels;

    void Start(){
		if (habilidad != null) {

			textHover = transform.GetChild(2).gameObject;


			Image image = GetImage(transform.GetChild(0));
			image.color = habilidad.backgroundColor;
			image.GetComponent<UnityEngine.UI.Outline>().effectColor = habilidad.outlineColor; //Bien

			TextMeshProUGUI kanji = transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
			kanji.color = habilidad.kanjiStrokeColor;
			kanji.text = habilidad.kanji;

			SetText(transform.GetChild(1).GetChild(2), habilidad.GetPrice().ToString());

			textHover.transform.GetComponent<Image>().color = habilidad.backgroundColor;
			textHover.SetActive(false);

			SetText(textHover.transform.GetChild(0), habilidad.Name);
			SetText(textHover.transform.GetChild(2), habilidad.element1.ToString());
			if (habilidad.element2 != ElementButton.None)
				SetText(textHover.transform.GetChild(3), habilidad.element2.ToString());
			else
				SetText(textHover.transform.GetChild(3), "");
			SetText(textHover.transform.GetChild(4), habilidad.skillDescription);
			SetText(textHover.transform.GetChild(6), habilidad.GetPrice().ToString());

			Transform levelsParent = transform.GetChild(1).GetChild(1);
			levels = new List<Image>() {
				GetImage(levelsParent.GetChild(0).GetChild(0)),
				GetImage(levelsParent.GetChild(0).GetChild(1)),
				GetImage(levelsParent.GetChild(2).GetChild(0)),
				GetImage(levelsParent.GetChild(2).GetChild(1))
			};
		}

		if (vida || mana) {
			SetText(transform.GetChild(1).GetChild(2),"200");
		}
    }

    PenguinResourcesMulti miPinguino;
    public void SetMyself() {
        Collider[] hits = Physics.OverlapSphere(GameManager.instance.tiendaActual.position, 5, LayerMask.GetMask("Clickable"));
        List<GameObject> hitteables = new List<GameObject>();
        foreach(Collider hit in hits) {
            if(hit.transform.TryGetComponent<PenguinResourcesMulti>(out PenguinResourcesMulti hitPen))
                hitteables.Add(hit.gameObject);
        }
        GameObject hitObject = Utils.GetNearestTarget(hitteables, transform.position);
        hitObject?.TryGetComponent<PenguinResourcesMulti>(out miPinguino);

    }

    private Image GetImage(Transform t) {
		return t.GetComponent<Image>();
	}
	private TextMeshProUGUI GetText(Transform t) {
		return t.GetComponent<TextMeshProUGUI>();
	}
	private void SetText(Transform t, string text) {
		t.GetComponent<TextMeshProUGUI>().text = text;
	}

	public void UpgradeSO() {
        if(miPinguino == null)
            SetMyself();

		if (miPinguino.SpendMoney(habilidad.GetPrice()) && habilidad.nivelHabilidad<5) {
            levels[habilidad.nivelHabilidad - 1].color = Color.cyan;
			habilidad.nivelHabilidad++;
			if (habilidad.nivelHabilidad == 5) {
				SetText(transform.GetChild(1).GetChild(2), "MAXED");
				SetText(textHover.transform.GetChild(6), "MAXED");

			} else {
				SetText(transform.GetChild(1).GetChild(2), habilidad.GetPrice().ToString());
				SetText(textHover.transform.GetChild(6), habilidad.GetPrice().ToString());
			}
		}

	}

    public void OnPointerEnter(PointerEventData eventData){
        if(eventData != null && textHover != null)
            textHover.SetActive(true);
    }
    

    public void OnPointerExit(PointerEventData eventData) {
        if(eventData != null && textHover != null)
            textHover.SetActive(false);      
    }

	public void AddHP() {

		if (miPinguino == null)
			SetMyself();

		TextMeshProUGUI a = GetText(transform.GetChild(1).GetChild(2));
		int val = int.Parse(a.text);
		if (miPinguino.SpendMoney(val)) {
			miPinguino.maxHp *= 1.1f;
			miPinguino.ResetHP();
			a.text = Mathf.FloorToInt(val*1.25f) + "";
		}
	}

	public void AddMana() {
		if (miPinguino == null)
			SetMyself();

		TextMeshProUGUI a = GetText(transform.GetChild(1).GetChild(2));
		int val = int.Parse(a.text);
		if (miPinguino.SpendMoney(val)) {
			miPinguino.maxMana *= 1.1f;
			miPinguino.ResetMana();
			a.text = Mathf.FloorToInt(val * 1.25f) + "";
		}
	}
}
