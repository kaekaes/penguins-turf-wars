using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Habilidad", order = 0)]
public class SOHabilidad : ScriptableObject{

	public string Name;
	public GameObject objectToSpawn;
    public ElementButton element1;
    public ElementButton element2;


    [Header("HUD")]
    public Color  backgroundColor;
    public string kanji;
    public Color  kanjiStrokeColor;
    public Color  outlineColor;
    [ColorUsage(true, true)] public Color staffColor;


	[Header("Stats")]
	public float manaCost = 1;
    public float damageHabilidad = 1;
    public int nivelHabilidad = 1;
	public bool mustSelectWhere = false;
	public float maxArea = 5f;
	public bool stillOnCast;
	public float moveAgainTime = 0;
	public float castAgainExtraTime = 0;
    public float moneyCostUpgrade = 0;
    public string skillDescription = "";


	[Header("Effcts")]
    [Tooltip("Tiempo que dura el debuff")] public float buffent = 3f;
    [Tooltip("Daño de la quemadura (Aplica 4 veces por segundo)")]public float quemadura = 2f;
    [Range(0, 1)] public float slow = 3f;
    [Range(1, 2)] public float masDamage = 3f;
    [Range(1, 2)] public float masVelocidad = 3f;  // la velocidad se aplica al pinguino que castea el ataque


	public override string ToString() {
		return JsonUtility.ToJson(this);
	}

	public int GetPrice() {
		return Mathf.FloorToInt(element2 != ElementButton.None ? 100 * nivelHabilidad * 1.5f : 50 * nivelHabilidad * 1.5f);
	}
}
