using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new Controles", order = 0)]
public class SOControles : ScriptableObject {

	//public SOPlayer _SESSION;

	public KeyCode Fire = KeyCode.Q;
	public KeyCode Water = KeyCode.W;
	public KeyCode Earth = KeyCode.E;
	public KeyCode Air = KeyCode.R;
	public KeyCode Cast = KeyCode.Space;
	public KeyCode Cancel = KeyCode.F;
	public KeyCode Move = KeyCode.Mouse1;
	public KeyCode Click = KeyCode.Mouse0;

	[NonSerialized]
    public Dictionary<ElementButton, KeyCode> keys = new Dictionary<ElementButton, KeyCode>();

	public void Prepare() {
		TryPrepare(ElementButton.Fire, Fire);
		TryPrepare(ElementButton.Water, Water);
		TryPrepare(ElementButton.Earth, Earth);
		TryPrepare(ElementButton.Air, Air);
		TryPrepare(ElementButton.Cast, Cast);
		TryPrepare(ElementButton.Cancel, Cancel);
		TryPrepare(ElementButton.Click, Click);
		TryPrepare(ElementButton.Move, Move);
	}
	public void HardPrepare() {
		ModifyKey(ElementButton.Fire, Fire);
		ModifyKey(ElementButton.Water, Water);
		ModifyKey(ElementButton.Earth, Earth);
		ModifyKey(ElementButton.Air, Air);
		ModifyKey(ElementButton.Cast, Cast);
		ModifyKey(ElementButton.Cancel, Cancel);
		ModifyKey(ElementButton.Click, Click);
		ModifyKey(ElementButton.Move, Move);
	}

	private void TryPrepare(ElementButton key, KeyCode value) {
        if(!keys.ContainsKey(key)) {
            ChangeVar(key, value);
            keys.Add(key, value);
        }
	}

	public void ModifyKey(ElementButton key, KeyCode value) {
        if(keys.ContainsKey(key)) 
            keys.Remove(key);

        keys.Add(key, value);
        ChangeVar(key, value);

    }

    public void ChangeVar(ElementButton key, KeyCode value) {
        switch(key) {
            case ElementButton.Air:
                Air = value;
                break;
            case ElementButton.Water:
                Water = value;
                break;
            case ElementButton.Earth:
                Earth = value;
                break;
            case ElementButton.Fire:
                Fire = value;
                break;
            case ElementButton.Cast:
                Cast = value;
                break;
            case ElementButton.Cancel:
                Cancel = value;
                break;
            case ElementButton.Move:
                Move = value;
                break;
            case ElementButton.Click:
                Click = value;
                break;
        }
    }

    public void Restart() {
        keys.Clear();

        Fire = KeyCode.Q;
        Water = KeyCode.W;
        Earth = KeyCode.E;
        Air = KeyCode.R;
        Cast = KeyCode.Space;
        Cancel = KeyCode.F;
        Move = KeyCode.Mouse1;
        Click = KeyCode.Mouse0;
        HardPrepare();
    }

    public override string ToString() {
		return JsonUtility.ToJson(this) ;
	}

}