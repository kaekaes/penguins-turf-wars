using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class Utils {

	public static GameObject GetNearestTarget(List<GameObject> p, Vector3 mypos) {
		GameObject[] list = SortList(p, mypos);
		if (list.Length == 0)
			return null;
		else
			return list[0];
	}

	public static GameObject[] SortList(List<GameObject> p, Vector3 mypos){
		return p.OrderBy((go) => (go.transform.position - mypos).sqrMagnitude).ToArray();
	}
}
