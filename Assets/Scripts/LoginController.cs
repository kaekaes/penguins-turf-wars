using System.Collections;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class LoginController : MonoBehaviour{

    const string LOGINURL = "http://atheroxportfolio.000webhostapp.com/login.php";
	const string REGISTERURL = "http://atheroxportfolio.000webhostapp.com/register.php";
	const string EXISTSURL = "http://atheroxportfolio.000webhostapp.com/exists.php";

	private Coroutine actualAction;
	private Coroutine actualMessage;

    public TMP_InputField user;
    public TMP_InputField pass;
	public UnityEngine.UI.Outline messageImg;
	private TextMeshProUGUI messageText;
    public GameObject cannotclick;
    public SOPlayer _SESSION;

	EventSystem system;

	void Start() { 
		system = EventSystem.current;
        cannotclick.SetActive(false);
		messageText = messageImg.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
		messageImg.gameObject.SetActive(false);
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Tab) && !Input.GetKeyDown(KeyCode.LeftShift)) {
			if (Input.GetKey(KeyCode.LeftShift)) {
				Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnLeft();
				if (next != null) {

					InputField inputfield = next.GetComponent<InputField>();
					if (inputfield != null)
						inputfield.OnPointerClick(new PointerEventData(system));

					system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
				} else {
					next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();
					if (next != null) {

						InputField inputfield = next.GetComponent<InputField>();
						if (inputfield != null)
							inputfield.OnPointerClick(new PointerEventData(system));

						system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
					}
				}

			} else {

				Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
				if (next != null) {

					InputField inputfield = next.GetComponent<InputField>();
					if (inputfield != null)
						inputfield.OnPointerClick(new PointerEventData(system));

					system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
				} else {
					next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnRight();
					if (next != null) {

						InputField inputfield = next.GetComponent<InputField>();
						if (inputfield != null)
							inputfield.OnPointerClick(new PointerEventData(system));

						system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
					}
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.Tab)) {
		}

	}

	public void LoginButton() {
        if(user.text == "" || pass.text == "")
            return;

        cannotclick.SetActive(true);
		actualAction = StartCoroutine(Login());
		if (actualMessage != null)
			StopCoroutine(actualMessage);
	}

	public void RegisterButton() {
        if(user.text == "" || pass.text == "") 
            return;
        
        cannotclick.SetActive(true);
		actualAction = StartCoroutine(Register());
		if(actualMessage != null)
			StopCoroutine(actualMessage);
	}

    IEnumerator Login() {
        yield return new WaitForEndOfFrame();

        WWWForm form = new WWWForm();
        form.AddField("User", user.text);
        form.AddField("Pass", Hasher(pass.text));
        using(var w = UnityWebRequest.Post(LOGINURL,form)) {
            yield return w.SendWebRequest();
            print(w.downloadHandler.text);
            if(w.result == UnityWebRequest.Result.Success) {
				if(w.downloadHandler.text == "" || w.downloadHandler.text.Substring(0, 1) != "{") {
					print("check user exists");
					WWWForm form2 = new WWWForm();
					form2.AddField("User", user.text);

					print("Checking que si existe el usuario");
					using (var w2 = UnityWebRequest.Post(EXISTSURL, form)) {
						yield return w2.SendWebRequest();
						if (w2.result == UnityWebRequest.Result.Success) {
							if (w2.downloadHandler.text != "") 
								SetMessage("Contraseña equivocada", Color.red);
							else
								SetMessage("El usuario no existe", Color.red);

						} else 
							SetMessage("ERROR DE CONEXIÓN, prueba de nuevo más tarde", Color.red);
					}
				} else {
					SetMessage("Te logiaste!", Color.green);
					Player a = JsonUtility.FromJson<Player>(w.downloadHandler.text);
					_SESSION.id = a.Id;
					_SESSION.username = a.User;
					_SESSION.lvl = a.Lvl;
					_SESSION.exp = a.Exp;
                    //Mover a otra escena
                    yield return new WaitForEndOfFrame();
                    SceneUtils.NextScene();
				}
                
            } else {
				SetMessage("Error en la CONEXIÓN con el servidor", Color.red);
				print(w.result);
            }
            cannotclick.SetActive(false);

        }
    }

    IEnumerator Register() {
        yield return new WaitForEndOfFrame();

        if(pass.text.Length < 3) {
            SetMessage("Error, pass must be 3 characters at least", Color.red);
            CancelActions();
        }

        bool userAlreadyExists = false;

		WWWForm form = new WWWForm();
		form.AddField("User", user.text);

		print("Checking que no existe el usuario");
		using (var w = UnityWebRequest.Post(EXISTSURL, form)) {
			yield return w.SendWebRequest();
			if (w.result == UnityWebRequest.Result.Success) {
				if (w.downloadHandler.text != "") {
					SetMessage("El usuario ya existe", Color.red);
					userAlreadyExists = true;
					CancelActions();
				}
			} else {
				SetMessage("ERROR DE CONEXIÓN, prueba de nuevo más tarde", Color.red);
				userAlreadyExists = true;
			}
		}


		if (!userAlreadyExists) {
			form = new WWWForm();
			form.AddField("User", user.text);
			form.AddField("Pass", Hasher(pass.text));

			print("El usuario no existe, procediendo a registrarlo");
			using (var w = UnityWebRequest.Post(REGISTERURL, form)) {
				yield return w.SendWebRequest();
				print(w.downloadHandler.text);
				if (w.result == UnityWebRequest.Result.Success) {
					if (w.downloadHandler.text == "Error")
						SetMessage("Fallo, la pass debe ser minimo 3 carácteres", Color.red);
					else if (w.downloadHandler.text == "Done")
						SetMessage("Usuario creado!", Color.green);
				} else
					print("ERROR");

				cannotclick.SetActive(false);
			}
        }
    }

	/// <summary>
	/// Encripta el string que le pases traspasando todos sus carácteres a los bytes correspondietes de la tabla ASCII, esa array la encripta 
	/// en SHA256 y la devuelve como un string.
	/// </summary>
	/// <param name="pass">Texto a encriptar</param>
	/// <returns></returns>
	public string Hasher(string pass) {
		byte[] passData = System.Text.Encoding.ASCII.GetBytes(pass);
		passData = new SHA256Managed().ComputeHash(passData);
		return System.Text.Encoding.ASCII.GetString(passData);
	}

	public void CancelActions() {
		StopCoroutine(actualAction);
		cannotclick.SetActive(false);
	}

	public void SetMessage(string message, Color color, float time = 2f) {
		messageImg.gameObject.SetActive(true);
		messageText.text = message;
		actualMessage = StartCoroutine(ShowMessage(time, color));
	}

	private Color alpha = new Color(0, 0, 0, 0);
	IEnumerator ShowMessage(float time, Color color) {
		for (float t = 1; t > 0; t -= Time.deltaTime) {
			messageImg.effectColor = Color.Lerp(color, alpha, t);
			messageText.color = Color.Lerp(Color.white, alpha, t);
			yield return null;
		}
		yield return new WaitForSeconds(2);
		for (float t = 0; t < 1; t += Time.deltaTime / time) {
			messageImg.effectColor = Color.Lerp(color, alpha, t);
			messageText.color = Color.Lerp(Color.white, alpha, t);
			yield return null;
		}
		messageImg.gameObject.SetActive(false);
	}

    [System.Serializable]
    class Player {
        public int Id;
        public string User;
        public int Lvl;
        public int Exp;

        public Player(int id, string user, int lvl, int exp) {
            User = user;
            Id = id;
            Lvl = lvl;
            Exp = exp;
        }

        public override string ToString() {
            return "ID:"+Id + ", User:" + User + ", Lvl:" + Lvl + ", Exp:" + Exp;
        }
    }
}
