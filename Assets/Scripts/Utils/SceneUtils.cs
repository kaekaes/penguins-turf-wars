using System;
using System.Collections;
using System.Collections.Generic;
using SharedLibrary;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Networking;

/// <summary>
/// Actua como una librera de acciones para botones y cosas
/// </summary>
public class SceneUtils : SingletonPersistent<SceneUtils> {
	
	public GameObject cannotClick;
	public SOControles controles;
	public Player player;


	private void Start() {
		print("a");
	}

	public static void GoToScene(int id) {
        SceneManager.LoadScene(id);
    }

    public static void LastScene() {
        int id = SceneManager.GetActiveScene().buildIndex;
		if (id > 0)
			SceneManager.LoadScene(id - 1);
    }

    public static void NextScene() {
        int id = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(id + 1);
    }

	// public void SaveControls() {
	// 	saveCoroutine = StartCoroutine(SaveControls(_SESSION));
	// }

 //    public static void CerrarSession(SOPlayer _SESSION) {
	// 	_SESSION.CerrarSesion(true);
 //    }
 //
	// /// <summary>
	// /// Antes de salir cierra sesin y despus sale
	// /// </summary>
 //    public void Exit() {
 //        _SESSION.CerrarSesion(false);
 //        StartCoroutine(ExitC());
 //    }
 //
	// /// <summary>
	// /// Salir es una corrutina porque hacemos que se espere a que haya guardado antes de salir
	// /// </summary>
	// /// <returns></returns>
	// IEnumerator ExitC() {
	// 	if (saveCoroutine != null)
	// 		yield return new WaitUntil(() => saveCoroutine == null);
 //
	// 	Application.Quit();
	// 	//Esto hace que como en el editor no puedes hacer quit simplemente pare el juego
	// 	#if UNITY_EDITOR
	// 		UnityEditor.EditorApplication.isPlaying = false;
	// 	#endif
	// 	yield return null;
	// }

	/*
	 * Estas son las funciones de buscar partida y todo eso, pero como dio tiempo hecho hemos hecho que darle a jugar
	 * te lleve a la escena del juego y debas darle al HUD de mirror :/
	 */
	#region BotonesJugar
	public static void SearchGame(){
    }

    public static void HostGame(){
    }

    public static void Volume(){
    }
	#endregion

	private bool anyKeyPressed = false, needToCheckKey = false;
    private KeyCode keyPressed;
    private Coroutine rutina;
	private Image rutineImage;
    public void ChangeKey() {
        controles.Prepare();
        GameObject a = EventSystem.current.currentSelectedGameObject;
        cannotClick?.SetActive(true);
        cannotClick.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Press KEY for " + a.name;

		if (TryGetComponent<ProfileManager>(out ProfileManager pm))
			pm.SetColor(a.name);
        a.TryGetComponent<Button>(out Button but);
		rutineImage = but.image;
		rutineImage.color = Color.green;

		rutina = StartCoroutine(GetActualKey(a));

    }

    IEnumerator GetActualKey(GameObject a) {
        yield return new WaitForEndOfFrame();
        needToCheckKey = true;
        if(anyKeyPressed) {
            print(keyPressed);
            ElementButton element = (ElementButton)(Enum.Parse(typeof(ElementButton), a.name));
            controles.ModifyKey(element, keyPressed);

			cannotClick.SetActive(false);

			rutineImage.color = Color.white;
            a.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = keyPressed.ToString();
            keyPressed = KeyCode.None;
            anyKeyPressed = false;
            needToCheckKey = false;
			if(TryGetComponent<ProfileManager>(out ProfileManager pm))
				pm.UnsetColor();
			yield return null;
        }else
            rutina = StartCoroutine(GetActualKey(a));
    }

    public void CancelCheckKey() {
        StopCoroutine(rutina);
		keyPressed = KeyCode.None;
		anyKeyPressed = false;
		needToCheckKey = false;
		cannotClick.SetActive(false);
		rutineImage.color = Color.white;
		if (TryGetComponent<ProfileManager>(out ProfileManager pm))
			pm.UnsetColor();
		Event.current = null;
	}

    private void OnGUI() {
        if(needToCheckKey) {
            Event e = Event.current;
            if(e.isKey) {
                keyPressed = e.keyCode;
                anyKeyPressed = true;
                needToCheckKey = false;
            }else if(e.isMouse) {
                if(Input.GetMouseButtonUp(0)) {
                    keyPressed = KeyCode.Mouse0;
                    anyKeyPressed = true;
                    needToCheckKey = false;
                } else if(Input.GetMouseButtonUp(1)) {
                    keyPressed = KeyCode.Mouse1;
                    anyKeyPressed = true;
                    needToCheckKey = false;
                } else if(Input.GetMouseButtonUp(2)) {
                    keyPressed = KeyCode.Mouse2;
                    anyKeyPressed = true;
                    needToCheckKey = false;
                }

            }
        }
    }

    public void ResetControls() {
        controles.Restart();
		GetComponent<ProfileManager>().PrepareKeys();

	}



	const string SETCONTROLESURL = "http://atheroxportfolio.000webhostapp.com/setControles.php";
	const string UPDATECONTROLESURL = "http://atheroxportfolio.000webhostapp.com/updateControles.php";
	const string GETCONTROLESURL = "http://atheroxportfolio.000webhostapp.com/getControles.php";
	const string EXISTSCONTROLESURL = "http://atheroxportfolio.000webhostapp.com/existsControles.php";
	public Coroutine saveCoroutine;

	public IEnumerator SaveControls(SOPlayer _SESSION) {
		Debug.Log(JsonUtility.ToJson(controles));
		WWWForm existsForm = new WWWForm();
		existsForm.AddField("IdJugador", _SESSION.id);
		bool exists = false;
		using (var w = UnityWebRequest.Post(EXISTSCONTROLESURL, existsForm)) {
			yield return w.SendWebRequest();

			if (w.result == UnityWebRequest.Result.Success) {

				Debug.Log(w.downloadHandler.text);
				Debug.Log(w.downloadHandler.isDone);

				if (w.downloadHandler.text.Contains("Done"))
					exists = true;
			} else {
				Debug.Log(" is HTTP error : " + w.result);
				Debug.Log(" error : " + w.error);
			}
		}

		if (exists) {
			WWWForm form = new WWWForm();

			form.AddField("IdJugador", _SESSION.id);
			form.AddField("Controles", JsonUtility.ToJson(controles));
			using (var w = UnityWebRequest.Post(UPDATECONTROLESURL, form)) {
				yield return w.SendWebRequest();

				if (w.result == UnityWebRequest.Result.Success) {
					if (w.downloadHandler.text.Contains("Done")) {
						Debug.Log("Ole");
					}
				}
			}
		} else {

			WWWForm form = new WWWForm();

			form.AddField("IdJugador", _SESSION.id);
			form.AddField("Controles", JsonUtility.ToJson(controles));
			using (var w = UnityWebRequest.Post(SETCONTROLESURL, form)) {
				yield return w.SendWebRequest();

				if (w.result == UnityWebRequest.Result.Success) {
					if (w.downloadHandler.text.Contains("Done")) {
						Debug.Log("Ole");
					}
				}
			}
		}

		saveCoroutine = null;
	}


	public IEnumerator GetControls(SOPlayer _SESSION) {

		WWWForm form = new WWWForm();
		if (_SESSION == null) 
			controles.Restart();
		else {
			form.AddField("IdJugador", _SESSION.id);
			using (var w = UnityWebRequest.Post(GETCONTROLESURL, form)) {
				yield return w.SendWebRequest();

				if (w.result == UnityWebRequest.Result.Success) {
					if (w.downloadHandler.text.Contains("Fire")) {
						Debug.LogWarning(w.downloadHandler.text);
						JsonUtility.FromJsonOverwrite(w.downloadHandler.text, controles);
						controles.HardPrepare();
					} else {
						controles.Restart();
					}
					if (TryGetComponent<ProfileManager>(out ProfileManager pm))
						pm.PrepareKeys();
				}
			}
			saveCoroutine = null;
		}
	}


    void OnApplicationQuit() {
        // player.CerrarSesion(false);
    }
}
