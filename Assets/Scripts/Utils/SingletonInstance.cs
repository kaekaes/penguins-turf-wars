using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonInstance<T> : MonoBehaviour where T : Component {
    public static T Instance {
        get {
            if (Instance == null) {
                var obj = FindObjectOfType(typeof(T)) as T[];
                if (obj.Length > 0)
                    Instance = obj[0];
                if (obj.Length > 1)
                    Debug.LogError($"More of {typeof(T).Name} in the Scene");
                if (Instance = null) {
                    GameObject o = new GameObject() {
                        hideFlags = HideFlags.HideAndDontSave
                    };
                    Instance = o.AddComponent<T>();
                }
            }

            return Instance;
        }
        private set { Instance = value; }
    }
}

public class SingletonPersistent<T> : MonoBehaviour where T : Component {
    public static T Instance { get; private set; }

    private void Awake() {
        if (Instance == null) {
            Instance = this as T;
            DontDestroyOnLoad(this);
        }
        else {
            Destroy(this.gameObject);
        }
    }
}