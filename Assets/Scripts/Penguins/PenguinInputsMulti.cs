using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Mirror;

public class PenguinInputsMulti: NetworkBehaviour {

    private PenguinControllerMulti manager;
    public void SetManager(PenguinControllerMulti manager) => this.manager = manager;

    [SyncVar] private byte skillSelected = 0b0000;
    [SyncVar] private int actualSkillCount = 0; // Si no se sincronizaba habían veces que algún cliente podía combinar 3 y explotaba
    private int maxSkillsCount = 2;				// Esto está puesto porque me gustaría en un futuro añadir una tercera capa

    private Transform shootPoint;				// Punto en el que se spawnea el padre de las animaciones
    private HudManager hudManager;				// Como se usa mucho preferimos hacer una referencia rapida aquí
    private EventSystem eventSystem;			// Más rápido que hacer EventSystem.current ya que no entra en external calls aunque haya que hacer un Find
    private Transform skillsButtonsParent;

    [SyncVar] public bool casting = false;
	[SyncVar] public bool canMove = false;
	public float canCastAgainTime = 0.5f;

    public GameObject objectWhereSkillsAreInstanced;


	void Start() {
        hudManager = GameManager.instance.hud;
		// Aunque consume es en un Start mientras carga la partida de forma que es muy importante
        eventSystem = (EventSystem)FindObjectOfType(typeof(EventSystem)); 

		shootPoint = transform.GetChild(1);

        skillsButtonsParent = GameManager.instance.hud.skillsButtonsParent;
		// Esto le mete a todos los botones del HUD sus acciones
        foreach(Transform t in skillsButtonsParent) {
            t.TryGetComponent<Button>(out Button tBut);
            tBut.onClick.AddListener(() => {
                AddElement(t.name);
            });
		}

		// En caso de que te reconectes esto hará que no tengas ninguna habilidad seleccionada
		if (isLocalPlayer)
			ResetHabilidad();
	}

	/// <summary>
	/// Añade un elemento a la combinación mientras sean menos de la cantidad máxima
	/// </summary>
	/// <param name="element">Elemento a añadir</param>
    public void AddElement(byte element) {
        if(actualSkillCount >= maxSkillsCount)
            return;

		// Aquí comprobamos que no estés añadiendo el mismo elemento 2 veces y por alguna razón se debe hacer en varios pasos
		byte r = skillSelected;
		r ^= element;
		if (r == 0b0) 
			return;

		// Añade el byte del nuevo elemento a la combinación
        skillSelected |= element;
        actualSkillCount++;
        DisplaySkills();
    }

    public void AddElement(string element) {
        switch(element) {
            case "Air":
                AddElement(Skills.AIR);
                break;
            case "Earth":
                AddElement(Skills.EARTH);
                break;
            case "Fire":
                AddElement(Skills.FIRE);
                break;
            case "Water":
                AddElement(Skills.WATER);
                break;
        }
    }

	/// <summary>
	/// Gestiona lo que son puslar las teclas de los elementos, castear y cancelar habilidades
	/// </summary>
    public void CheckInputs() {
		// Usa como tecla el botón que le toque dependiendo de lo que diga el SOControles que es la tecla asignada a Fire
        if(Input.GetKeyDown(manager.controles.keys[ElementButton.Fire]))
			// Esto hace pasar el click como si fuese una pulsación en el botón del HUD que le toca
            ExecuteEvents.Execute(skillsButtonsParent.GetChild(0).gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);

        if(Input.GetKeyDown(manager.controles.keys[ElementButton.Water]))
            ExecuteEvents.Execute(skillsButtonsParent.GetChild(1).gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);


        if(Input.GetKeyDown(manager.controles.keys[ElementButton.Earth]))
            ExecuteEvents.Execute(skillsButtonsParent.GetChild(2).gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);


        if(Input.GetKeyDown(manager.controles.keys[ElementButton.Air]))
            ExecuteEvents.Execute(skillsButtonsParent.GetChild(3).gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);


		if (Input.GetKeyDown(manager.controles.keys[ElementButton.Cast])) {
            SOHabilidad act = GetActualSkill();
            if(act == null) return;												// Si le das a la tecla de Cast y no tienes habilidad seleccionada no hace nada
            Ray ray = manager.myCamera.ScreenPointToRay(Input.mousePosition);	// En caso de si tener lanza un rayo
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100, manager.terrainMask)) {		// Solamente comprueba en la layer de cosas seleccionables, esto incluye el circulo
				Vector3 dir = new Vector3(hit.point.x, transform.position.y, hit.point.z); // de selección, los enemigos, torres, nexos, pingüinions, etc
				transform.LookAt(dir);											
				CastSkill(dir, act.mustSelectWhere);							// Llama a la función de crear la habilidad diciendole donde
			}
		}

		if (Input.GetKeyDown(manager.controles.keys[ElementButton.Cancel]))
			ResetHabilidad();
	}

	public void Update() {
		if(!isLocalPlayer) return;
		if (Input.GetKeyDown(KeyCode.Escape))
			GameManager.instance.hud.OpenEscapeMenu();
	}

	/// <summary>
	/// Comprueba si tiene capacidad de lanzar hablidades comprobando si tiene alguna seleccionada
	/// o si no está lanzando una ya
	/// </summary>
	/// <returns>True si peude lanzar habilidad</returns>
	public bool CanCastSkill() {
        return actualSkillCount != 0 && !casting;
    }

    public void ResetHabilidad() {
		if (!isLocalPlayer) return;
		actualSkillCount = 0;
        skillSelected = 0b0;
		if(hudManager != null)
        hudManager.SetSkillToHud(skillSelected);
		StartCoroutine(Reescalar(manager.penguinResources.selectableArea, Vector3.zero, .25f));

	}

	/// <summary>
	/// El cliente decide lanzar una habilidad, comprueba si puede o no lanzar una habilidad
	/// Después Consume el Mana y lanza en el servidor la habilidad
	/// </summary>
	/// <param name="dir">Dirección hacia la que se lanza la habilidad</param>
	/// <param name="selectedWhere">Si es True significa que no es la Dirección sino la posición de lanzamiento</param>
	[Client]	
    public void CastSkill(Vector3 dir, bool selectedWhere = false) {
        //Comprobamos que tenga habilidad seleccionada y no está lanzando ya una
        if(!CanCastSkill())
            return;

		// Comprobamos que tenga mana para ello (está separado en dos pasos porque si ya no puede lanzarla
		// no es necesario molestar al manager y eso significa menos accesos y menor consumo)
		SOHabilidad actSkill = GetActualSkill();
        if(!manager.penguinResources.HasMana(actSkill.manaCost))
            return;

        // Le decimos que ya estamos casteando y para que no pueda lanzar otra habilidad
        casting = true;

		// Hacemos que crezca el circulo de selección y que empiece el cooldown
		StartCoroutine(Reescalar(manager.penguinResources.selectableArea, Vector3.zero, .25f));
		Invoke(nameof(onCanCastAgain), canCastAgainTime + actSkill.castAgainExtraTime);
		// Si la habilidad nos obliga a quedarnos quietos durante el lanzamiento le decimos que no puede moverse durante el tiempo que diga la habilidad
		if (actSkill.stillOnCast) {
			onCannotMove();
			Invoke(nameof(onCanMove), actSkill.moveAgainTime);
		}

        // Le decimos al servidor que cree la habilidad en la posición que ha sido lanzada y
        CastSkillOnServer(skillSelected, dir, selectedWhere);

        // Reiniciamos la habilidad
        ResetHabilidad();
    }

	/// <summary>
	/// La habilidad lanzada en local la envia al servidor y de ahí a todos los clientes
	/// </summary>
	/// <param name="skillSelected">Byte de la skill lanzada</param> Esto es así porque no se le puede pasar la SOHabilidad entera
	/// <param name="dir">Dirección hacia la que se lanza la habilidad</param>
	/// <param name="selectedWhere">Si es True significa que no es la Dirección sino la posición de lanzamiento</param>
	[Command]
    public void CastSkillOnServer(byte skillSelected, Vector3 dir, bool selectedWhere) {

		// Pillamos que SO es según el byte
		SOHabilidad actSkill = GetActualSkill(skillSelected);

        // Creamos un objeto que contrendrá la habilidad y la ponemos dentro del skill
        GameObject padre = Instantiate(this.objectWhereSkillsAreInstanced);
        padre.transform.position = selectedWhere ? dir : shootPoint.position;
        padre.transform.LookAt(dir);

        //Después creamos las particulas y las ponemos dentro de padre y le decimos que habilidad es la que es realmente
        GameObject particles = Instantiate(actSkill.objectToSpawn, padre.transform);
        particles.transform.SetParent(padre.transform);
        particles.TryGetComponent<CollisionsHabilidades>(out CollisionsHabilidades partCol);
		partCol.skill = actSkill;
        partCol.myPlayer = transform;

		// Creamos estos objetos en todos los clientes pero algunas cosas no están sincronizadas porque jaja Mirror
		// Así que se lo pasamos a un ClientRPC y que ahí lo haga él
        NetworkServer.Spawn(padre);
		NetworkServer.Spawn(particles);
		SpawnPadreYParticle(actSkill, manager, particles,dir,selectedWhere);
    }



	/// <summary>
	/// Sincroniza las cosas no se sincronizas automáticamente en todos los clientes
	/// </summary>
	/// <param name="actSkill">SO de la habilidad lanzada</param>
	/// <param name="manager">El Manager del pinguino que lo está lanzando</param>
	/// <param name="particles">Objeto que crea el SOHabilidad</param>
	/// <param name="dir">Dirección hacia la que se lanza la habilidad</param>
	/// <param name="selectedWhere">Si es True significa que no es la Dirección sino la posición de lanzamiento</param>
	[ClientRpc]
	void SpawnPadreYParticle(SOHabilidad actSkill, PenguinControllerMulti manager, GameObject particles, Vector3 dir, bool selectedWhere) {
		// Si quién está ejecutando este trozo es el que la ha lanzado, le resta el maná
		if(hasAuthority)
			manager.penguinResources.ConsumeMana(actSkill.manaCost);

		// Le añade al pinguino que ha lanzado la habilidad sus bufos (en caso de aire y agua, eso ya lo controla la función de SetBuff)
		particles.TryGetComponent<CollisionsHabilidades>(out CollisionsHabilidades partCol);
        manager.penguinResources.SetBuff(actSkill.element1, actSkill);
        manager.penguinResources.SetBuff(actSkill.element2, actSkill);

		// Le decimos quien ha lanzado la habilidad
        partCol.myPlayer = transform;

		// Esto impide que el servidor ejecute esta parte porque él ya la ejecutó en la función anterior
		if (NetworkServer.active) return;

		// Creamos un objeto que contrendrá la habilidad y la ponemos dentro del skill (igual que en el servidor)
		GameObject padre = Instantiate(this.objectWhereSkillsAreInstanced);
		padre.transform.name = actSkill.name;
		padre.transform.position = selectedWhere ? dir : shootPoint.position;
		padre.transform.LookAt(dir);
		particles.transform.SetParent(padre.transform);
    }

	[Client]
	private void onCanCastAgain() {
		if (!isLocalPlayer) return;
		casting = false;
	}

	[Client]
	private void onCannotMove() {
		if (!isLocalPlayer) return;
		//print(transform.name + " cannot move");
		manager.agent.SetDestination(transform.position);
		canMove = false;
	}

	[Client]
	private void onCanMove() {
		if (!isLocalPlayer) return;
		//print(transform.name + " can move");
		canMove = true;
	}

	private void DisplaySkills() {
		if (!isLocalPlayer) return;
		SOHabilidad hab = hudManager.SetSkillToHud(skillSelected);
		if (hab == null)
			return;

		StartCoroutine(Reescalar(manager.penguinResources.selectableArea, new Vector3(hab.maxArea, .1f, hab.maxArea), .25f));		
    }

	/// <summary>
	/// Reescala el objeto que le pases en el tiempo determinado
	/// </summary>
	/// <param name="obj">Objeto a reescalar</param>
	/// <param name="size">Tamaño al que reescalar</param>
	/// <param name="time">Tiempo que tarda en hacerlo</param>
	/// <returns></returns>
	IEnumerator Reescalar(GameObject obj, Vector3 size, float time) {
        if(obj != null) {
            Vector3 acts = obj.transform.localScale;
			// Este for va de 0 a 1 para que funcione perfecto con Lerps de cualquier tipo
			// Funciona de forma que se ejecuta una vez cada frame y durante el tiempo que le pases por la variable *time* gracias al yield return
            for(float t = 0f; t < 1; t += Time.deltaTime / time) {
                obj.transform.localScale = Vector3.Lerp(acts, size, t);
                yield return null;
            }
        }
	}

	public SOHabilidad GetActualSkill() {
		return GameManager.instance.hud.GetSkillByByte(skillSelected);
	}
	public SOHabilidad GetActualSkill(byte skillSelected) {
		return GameManager.instance.hud.GetSkillByByte(skillSelected);
	}
}
public enum ElementButton { Fire, Water, Earth, Air, Cast, Cancel, Click, Move, None }
