/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Esta clase gestiona todo del pinguino, desde movimiento a seleccin, inputs, vida, etc.
/// </summary>
public class PenguinControllerMulti : NetworkBehaviour {
	
	//Slo la pillamos para que desde este script y desde el de inputs sea ms fcil y sean menos accesos a GameManager
	[HideInInspector] public Camera myCamera;

	#region Hooks
	[Header("Hooks")]
	[SyncVar] public bool imBlue = true;
    [SyncVar] public float DMGBase = 1.5f;
    [SyncVar(hook = nameof(HookSpeed))]
    public float speed = 1.5f;
	// Esto hookea la velocidad de forma que cambia la velocidad del agent al modificarse la variable speed (aunque creo que no era necesario sincronizarla)
    public void HookSpeed(float inutil, float actSpeed) {
		speed = actSpeed;
		if(agent!=null)
			agent.speed = speed;
	}
	#endregion

	#region Referencias internas a otros scripts
	[HideInInspector] public PenguinInputsMulti penguinInputs;
	[HideInInspector] public PenguinResourcesMulti penguinResources;
	[HideInInspector] public NexusController myNexus;
	#endregion

	#region Movimiento y Raycast
	[Header("Movimiento y Raycast")]
	public NavMeshAgent agent;
	private LayerMask clickableMask;				// Esta layer solo contiene los componentes clicables como enemigos, torres, tienda y cosas del estilo
	public LayerMask allWithoutClickableMask;		// Esta contiene todo menos ignoreRaycast y la anterior
	[HideInInspector] public LayerMask terrainMask;	// Solo contiene el suelo
	public float distanceOfInteractions = 5f;
	[HideInInspector] public GameObject clickPointerToMove;
	#endregion

	[Space]

	public Light staffLight;
	public SOControles controles;

	private bool moving = false;
	// Con este sistema de accesor hacemos que cuando pasas por encima el ratn de un objeto se le active su outline
	private Outline lastSelectedObject;
	private Outline SelectObject {
		get { return lastSelectedObject; }
		set {
			if (lastSelectedObject != null && lastSelectedObject != GetComponent<Outline>())
				lastSelectedObject.enabled = false;

			if (value != null) {
				lastSelectedObject = value;
				lastSelectedObject.enabled = true;
			}
		}
	}



	void Start() {
		// En caso de que de alguna forma los controles no están cargados se recargan
		controles.Prepare();

		TryGetComponent<PenguinInputsMulti>(out penguinInputs);
		if (penguinInputs != null) penguinInputs.SetManager(this);
		TryGetComponent<PenguinResourcesMulti>(out penguinResources);
		if (penguinResources != null) {
			penguinResources.SetManager(this);
			penguinResources.FullReset();
		}
		myNexus = GameManager.instance.GetNexusController(imBlue);
		TryGetComponent<NavMeshAgent>(out agent);
        speed = agent.speed;

		if (isLocalPlayer) {
			myCamera = GameManager.instance.mainCamera;
			myCamera.transform.GetComponent<FollowTarget>().target = transform;

			clickPointerToMove = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            clickPointerToMove.transform.name = transform.name + "pointer";
            clickPointerToMove.layer = LayerMask.GetMask(LayerMask.LayerToName(1));
			penguinInputs.ResetHabilidad();
        }

		clickableMask = LayerMask.GetMask("Clickable");
		terrainMask = LayerMask.GetMask("Terrain");

        GameManager.instance.hud.staffLight = staffLight;
    }

	void Update() {
		// En caso de no ser el jugador local, que la partida no haya empezado o no estar vivo no puedes hacer nada
		if (!isLocalPlayer || !GameManager.instance.gameStarted) return;
        if(!penguinResources.Alive()) return;

		// Todos los "updates" de los inputs y de los recursos vienen de este Update, de esta forma si no puedes moverte
		// Por alguna de las razones anteriores no puedes hacer nada de nada

		LocalMovementAndCastsManager();
		if (penguinInputs != null) penguinInputs.CheckInputs();
		if (penguinResources != null) penguinResources.Regenerate();
		if (moving)
			if (agent.remainingDistance < agent.stoppingDistance)
				moving = false;
	}


	/// <summary>
	/// Comprueba todo lo que es click derecho y click izquierdo
	/// En caso de estar apuntando a un objeto clicable como un enemigo te sale el outline. Si le das click izquierdo
	/// y tienes una habilidad seleccionada la lanzas hacia él (o sobre él), si no la tienes no hace nada (me gustaría 
	/// implementar que te acerques y la lance automáticamente).Si en cambio das click derecho te acercas a él.
	/// Si a parte de un objeto clickable es un Actionable y estás a rango haces su Act, esto hará Invoke a su lista de acciones. (ej. la tienda)
	/// 
	/// Si no es un objeto clickable puedes mantener click derecho para moverte o dar clicks izquierdos para ello.
	/// Existen ambos porque el izquierdo es más preciso a cambio de no poder mantenerlo.
	/// </summary>
	private void LocalMovementAndCastsManager() {
		Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, 100, clickableMask)) {
            //Marcar coso hovereado
            SelectObject = hit.collider.GetComponent<Outline>();
            if(Input.GetKeyDown(controles.keys[ElementButton.Click])) {
				if (hit.collider.TryGetComponent<Actionable>(out Actionable actionable)) {
					if (Vector3.Distance(hit.collider.transform.position, transform.position) <= distanceOfInteractions) 
						actionable.Act();
					else
						Move(hit.point);
				} else {
					SOHabilidad act = penguinInputs.GetActualSkill();
					if (act == null)
						return;
					// Esta invoca la magia donde clicas (por eso el true en el CastSkill) (ej. la roca)
					if (act.mustSelectWhere) {
						// esto se hace en un nuevo vector en vez de pasarle el hit.point porque o sino caería a la altura de del objeto, no funny.
						Vector3 dir = new Vector3(hit.point.x, transform.position.y, hit.point.z);
						if (Vector3.Distance(transform.position, dir) <= penguinResources.selectableArea.transform.localScale.x / 2) {
							transform.LookAt(dir);
							penguinInputs.CastSkill(dir, true);
						}
					} else { // Este es el que lanza la habilidad hacia el enemigo (ej. bola de fuego)
						if (hit.collider.transform.parent != transform) {
							/* 
							 * Requiere del MeshRenderer porque si lo hicieramos de la manera normal no podría clicar a un objeto
							 * Que no está complemtante dentro del area de lanzamiento, al pillar su MeshRenderer podemos sacar su bound y de ahí sacar
							 * El radio en base a su ancho y determinar que si está la mitad dentro nos vale. 
							 */

							hit.collider.TryGetComponent<MeshRenderer>(out MeshRenderer enMR);
							if (enMR == null)
								hit.collider.transform.GetChild(0).TryGetComponent<MeshRenderer>(out enMR);

							// En vez del hit.point usamos el collider.transform para que sea el centro del objeto
							Vector3 dir = new Vector3(hit.collider.transform.position.x, transform.position.y, hit.collider.transform.position.z);
							if (Vector3.Distance(transform.position, dir) - (enMR.bounds.size.x / 2) <= penguinResources.selectableArea.transform.localScale.x / 2) {
								transform.LookAt(dir);
								penguinInputs.CastSkill(dir);
							}
						} else
							Move(hit.point);
					}
				}
            } else if(Input.GetKey(controles.keys[ElementButton.Move])) {
				// Si tienes el ratón encima de un objeto del HUD no funciona el moverte.
                if(UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    return;

                if(Physics.Raycast(ray, out hit, 100, allWithoutClickableMask))
					Move(hit.point);

			}
		} else {
			SelectObject = null;
			if (Input.GetKeyDown(controles.keys[ElementButton.Click]) || Input.GetKey(controles.keys[ElementButton.Move])) {
				if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
					return;

				if (Physics.Raycast(ray, out hit, 100, allWithoutClickableMask)) 
					Move(hit.point);
				
			}
		}
	}

	/// <summary>
	/// El terminar la partida está en un ClientRPC para que a todo el mundo se le abra la ventana.
	/// Dependiendo de si has ganado sale el texto de que has ganado
	/// También te desconecta del servidor al acabar. (Nos soluciona un par de problemas)
	/// </summary>
	/// <param name="blue">Color del equipo perdedor</param>
	[ClientRpc]
    public void EndGame(bool blue) {
		if (!isLocalPlayer) return;
		if (imBlue != blue)
			GameManager.instance.hud.IHaveWonPutTheWinningTextPlz();
		if (isServer)
			NetworkManager.singleton.StopServer();
		NetworkManager.singleton.StopClient();
        GameManager.instance.hud.endGameGO.SetActive(true);
    }

	[ClientRpc]
	public void PlayerLeave() {
		if (!isLocalPlayer) return;
		GameManager.instance.hud.SomeoneLeave();
		if (isServer)
			NetworkManager.singleton.StopServer();
		NetworkManager.singleton.StopClient();
		GameManager.instance.hud.endGameGO.SetActive(true);
		SceneUtils.LastScene();
	}

	/// <summary>
	/// Puedes decirle al agent que se mueva o se teletransporte al punto que le digas
	/// </summary>
	/// <param name="targetPosition">Posicion a la que debe moverse</param>
	/// <param name="teleport">(Default: False) En caso de true se teletransporta en vez de moverse</param>
	public void Move(Vector3 targetPosition, bool teleport = false) {
		if (teleport) {
			agent.Warp(targetPosition);
		} else {
			if (!penguinInputs.canMove)
				return;
			clickPointerToMove.transform.position = targetPosition;
			agent.SetDestination(targetPosition);
			moving = true;
		}
	}

	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, distanceOfInteractions);
	}
}
