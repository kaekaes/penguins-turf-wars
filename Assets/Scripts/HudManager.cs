using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Gestiona todo lo que es el HUD, barras de vida y mana, habilidades, etc
/// </summary>
public class HudManager: MonoBehaviour {

    private Color alpha = new Color(0, 0, 0, 0);
    public Material staffGem;
    [HideInInspector] public Light staffLight;
    public Material selectableArea;
    public Image btnActualSkill;
    public TextMeshProUGUI txtActualSkill;

    public Slider hpBar;
    public Slider manaBar;
    public TextMeshProUGUI hpBarVal;
    public TextMeshProUGUI manaBarVal;
    public TextMeshProUGUI moneyValTxt;

    public Transform skillsButtonsParent;

    public GameObject loadingScreen;
    public TextMeshProUGUI findingPlayers;
    public GameObject endGameGO;
    public TextMeshProUGUI winLoseText;
	public GameObject escapeMenu;

    [System.Serializable]
    public class Habilidades {
        public string Name;
        public SOHabilidad habilidad;
        public byte myByte;
    }
    public List<Habilidades> habilidades;

    void Start() {
        foreach(Habilidades hab in habilidades) {
            hab.habilidad.nivelHabilidad = 1;
        }
    }

	public void IHaveWonPutTheWinningTextPlz() => winLoseText.text = "GANADO";
	public void SomeoneLeave() => winLoseText.text = "El otro jugador ha abandonado";

	/// <summary>
	/// A partir del byte que le pasas pone en el HUD su kanji, colores, etc.
	/// Tambin cambia la luz y el color del bculo
	/// </summary>
	/// <param name="skill">Skill a poner en el HUD</param>
	/// <returns>La habilidad a la cual pertenece ese byte</returns>
	public SOHabilidad SetSkillToHud(byte skill) {
        SOHabilidad habilidad = GetSkillByByte(skill);

        if(habilidad == null) {
            btnActualSkill.color = alpha;
            btnActualSkill.GetComponent<UnityEngine.UI.Outline>().effectColor = alpha;

            txtActualSkill.color = alpha;
            txtActualSkill.text = "";

            staffGem.color = Color.white;
			if(staffLight != null)
				staffLight.color = Color.white;
        } else {
            btnActualSkill.color = habilidad.backgroundColor;
            btnActualSkill.GetComponent<UnityEngine.UI.Outline>().effectColor = habilidad.outlineColor; //Bien

            txtActualSkill.color = habilidad.kanjiStrokeColor;
            txtActualSkill.text = habilidad.kanji;
            staffGem.color = habilidad.staffColor;
			if (staffLight != null)
				staffLight.color = habilidad.staffColor;
            selectableArea.SetColor("_Color", habilidad.staffColor);
        }
        return habilidad;
    }

	/// <summary>
	/// Busca en la lista de habilidades cual comparte el byte con el que ha recibido y devuelve su SO
	/// </summary>
	/// <param name="skillByte"></param>
	/// <returns></returns>
    public SOHabilidad GetSkillByByte(byte skillByte) {
        if(skillByte == 0b0)
            return null;
        return habilidades.Find(bite => bite.myByte == skillByte).habilidad;
    }

	/// <summary>
	/// Devuelve qu byte pertenece al SO que recibe
	/// </summary>
	/// <param name="skill"></param>
	/// <returns>Byte de la skill</returns>
    public byte GetByteOfSkill(SOHabilidad skill) {
        return habilidades.Find(habi => habi.habilidad == skill).myByte;
    }

    public void UpdateHP(float hp, float maxHp) {
        hpBar.value = hp / maxHp;
		//Esto lo que hace es escribe como texto cuanta vida tienes pero slo con ints (no salen comas) y le pone como mnimo 0
        hpBarVal.text = string.Format("{00:0}", Mathf.Max(0, Mathf.FloorToInt(hp))) + " / " + string.Format("{00:0}", maxHp);
    }

    public void UpdateMana(float mana, float maxMana) {
        manaBar.value = mana / maxMana;
        manaBarVal.text = string.Format("{00:0}", Mathf.Max(0, Mathf.FloorToInt(mana))) + " / " + string.Format("{00:0}", maxMana);
    }

    public void UpdateMoney(int money) => moneyValTxt.text = money + "";
    
	/// <summary>
	/// Muestra o esconde la pantalla de carga
	/// </summary>
    public void ShowLoadingScreen(bool state = true) => loadingScreen.SetActive(state);

    public void SetLoadingScreenMessage(string message) => findingPlayers.text = message;

	/// <summary>
	/// Termina la partida y dependiendo del bool que recibe gana uno u otro
	/// </summary>
	/// <param name="blue">Equipo ganador, true = Azul</param>
    public void FinPartida(bool blue) {
        GameManager.instance.gameStarted = false;
        GameManager.instance.server.bluePen?.EndGame(blue);
        GameManager.instance.server.redPen?.EndGame(blue);
    }

	public void OpenEscapeMenu() => escapeMenu.SetActive(!escapeMenu.activeSelf);
	
}
