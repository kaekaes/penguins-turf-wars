using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Clase que funciona como un OnClick y delegado para objetos del mapa
/// til para la tienda y objetos actuables como puzles o tutorial
/// </summary>
public class Actionable : MonoBehaviour{

	public UnityEvent OnClick;

	/// <summary>
	/// Llama a todas las funciones que tiene acumuladas en el evento
	/// </summary>
	public void Act() {
		OnClick?.Invoke();
	}
}
