﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;

public class Tower : HittableMulti {

	[Header("Propiedades")]
	public int damage = 2;

	public float distanceToAttack = 10f;
	private LayerMask clickableMask;

	public bool imBlue;

	private bool haveIBeenHitted;
	private Coroutine iHaveBeenHitted;
	private HittableMulti objetivoAApalizar;


	void Start() {
		ResetHP();
		clickableMask = LayerMask.GetMask("Clickable");
		StartCoroutine(CheckForNextWaypoint());
	}


	IEnumerator CheckForNextWaypoint() {
		while (true) {
			if (CheckForAttack())
				AttackGrr();
			yield return new WaitForSeconds(0.1f);
		}
	}


	public bool CheckForAttack() {
		Collider[] hits = Physics.OverlapSphere(transform.position, distanceToAttack, clickableMask);
        List<GameObject> hitteables = new List<GameObject>();
		foreach (Collider hit in hits) {
			if (hit.transform.TryGetComponent<PenguinResourcesMulti>(out PenguinResourcesMulti hitEn))
				if (hitEn.manager.imBlue != imBlue)
					hitteables.Add(hitEn.gameObject);

			if (hit.transform.TryGetComponent<Enemy>(out Enemy hitEne))
				if (hitEne.imBlue != imBlue)
					hitteables.Add(hitEne.gameObject);
		}

		GameObject hitObject = Utils.GetNearestTarget(hitteables, transform.position);
		if (hitObject != null) {
			hitObject.TryGetComponent<HittableMulti>(out objetivoAApalizar);
			print(objetivoAApalizar);
			return true;
		} else
			return false;
	}


	private void AttackGrr() {
		if (Vector3.Distance(transform.position, objetivoAApalizar.transform.position) <= distanceToAttack) {
			objetivoAApalizar.Hit(damage, this);
		}
	}


    public override bool Hit(float dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
        base.Hit((double)dmg, lastHitter, skipInvulnerable);

        Color hitColor = Color.red * Mathf.Pow(2, 1.2f);
        DamageEffectSequence(hitColor, .5f);
        StartCoroutine(DamageEffectSequenceOnServer(transform.GetChild(0).GetComponent<MeshRenderer>(), hitColor, .5f));
        return Alive();
    }

	/// <summary>
	/// Torre destruida
	/// </summary>
	public override void Die(HittableMulti lastHitter) {
        NetworkServer.Destroy(gameObject);
        Destroy(this.gameObject);

	}

	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position + new Vector3(0, .25f, 0), distanceToAttack);
	}
}
