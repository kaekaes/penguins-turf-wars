﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;

public class Enemy : HittableMulti{

    [HideInInspector] public Vector3 offsetToShoot; //Offset para que la torreta no dispare al centro
    Action actionOnEndOfPath; // Es una función por lambda que le entra por EnemySpawner

    [Header("Propiedades")]	
	public int damage = 2;
	public float atackCooldown = 1f;
	public float rangeOfAtack = .5f;
    public int moneyToDrop = 50;
    public bool imBlue;
    public float augmentRadi = 5f;
    
    private NavMeshAgent agent;

	public EnemySpawn whoSpawnedMe;
    public Vector3 target;
    public int roundWhereIsSpawned = 0;
    private int wavepointIndex = 0;

    public float distanceToGoToTheNextWayPoint = 0.3f;
    public float distanceToGoToAttack = 1f;
    public float distanceToAttack = 0.25f;
    private LayerMask clickableMask;

    private HittableMulti hitTarget;
    private PenguinResourcesMulti quienMePega;

    void Start(){}

    public void SpawnEnemy() {
        TryGetComponent<NavMeshAgent>(out agent);
        ResetHP();
        agent.enabled = true;
        clickableMask = LayerMask.GetMask("Clickable");
        EndPath();
        GetNextWaypoint();

        StartCoroutine(CheckForNextWaypoint());

    }

	[ClientRpc]
	public void ChangeOutlineColor(bool imBlue) {
		transform.GetComponent<Outline>().OutlineColor = imBlue ? Color.blue : new Color(1f, 0.5f, 0);
	}

    IEnumerator CheckForNextWaypoint(){
        while(true) {
            if(!GameManager.instance.gameStarted)
                break;
            CheckHP();
            if(CheckEnemy())
                CheckForAttack();

            if (Vector3.Distance(transform.position, target) <= distanceToGoToTheNextWayPoint)
                GetNextWaypoint();

            yield return new WaitForSeconds(0.1f);
        }
        agent.SetDestination(transform.position);
    }

    private void CheckHP(){
        if (!Alive())
            Destroy(this.gameObject);
        
    }

    public bool CheckEnemy(){
        Collider[] hits = Physics.OverlapSphere(transform.position, distanceToGoToAttack, clickableMask);
		List<GameObject> hitteables = new List<GameObject>();
		foreach (Collider hit in hits) {
			//print(hit.transform.name);
			if (hit.transform.TryGetComponent<Enemy>(out Enemy hitEn)) 
				if (hitEn.imBlue == imBlue || !hitEn.Alive())
					continue;

			if (hit.transform.TryGetComponent<PenguinResourcesMulti>(out PenguinResourcesMulti hitPen)) 
				if (hitPen.manager.imBlue == imBlue || !hitPen.Alive())
					continue;

			if (hit.transform.TryGetComponent<NexusController>(out NexusController hitNe))
				if (hitNe.imBlue == imBlue || !hitNe.Alive())
					continue;

			if (hit.transform.TryGetComponent<Tower>(out Tower hitTow))
				if (hitTow.imBlue == imBlue || !hitTow.Alive())
					continue;


			hit.transform.TryGetComponent<HittableMulti>(out HittableMulti hitted);

			if (hitted == null || !hitted.Alive())
				continue;
			else 
				hitteables.Add(hit.gameObject);
            
        }

        GameObject hitObject = Utils.GetNearestTarget(hitteables,transform.position);
        hitObject?.TryGetComponent<HittableMulti>(out hitTarget);

        //if (agent == null)
        //	return false;
        if(agent.isOnNavMesh) {
            if(hitObject != null) {
                agent.SetDestination(hitTarget.transform.position);
                return true;
            } else {
                hitTarget = null;
                agent.SetDestination(target);
                return false;
            }
        }
        return false;
	}

	public override bool Hit(float dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
		base.Hit((double)dmg,lastHitter, skipInvulnerable);
        if(lastHitter.GetType() == typeof(PenguinResourcesMulti))
            quienMePega = (PenguinResourcesMulti)lastHitter;

        Color hitColor = Color.red * Mathf.Pow(2, 1.2f);
		DamageEffectSequence(hitColor, .5f);
		StartCoroutine(DamageEffectSequenceOnServer(transform.GetChild(0).GetComponent<MeshRenderer>(), hitColor, .5f));
		return Alive();
	}

	public void CheckForAttack() {
        if(Vector3.Distance(transform.position, hitTarget.transform.position) <= distanceToAttack) {
            hitTarget.Hit(damage, this);
        }
	}

	/// <summary>
	/// Va al siguiente punto, si no hay un siguiente termina el path y se destruye
	/// </summary>
	void GetNextWaypoint(){
        if (wavepointIndex >= whoSpawnedMe.oleadas[roundWhereIsSpawned].waypoints.points.Length - 1){
            EndPath();
            return;
        }
        wavepointIndex++;
        target = whoSpawnedMe.oleadas[roundWhereIsSpawned].waypoints.points[wavepointIndex];
        agent.SetDestination(target);
        
    }

    /// <summary>
    /// Si tiene una función en actionOnEndOfPath la ejecuta y después se destruye
    /// </summary>
    void EndPath(){
        if (actionOnEndOfPath != null)
            actionOnEndOfPath();
		
        //hacer que hitte al nexo 
    }

    /// <summary>
    /// Función que ejecuta al morir
    /// </summary>
    /// <param name="a">Función a ejecutar</param>
    public void SuscribeOnDead(Action a) {
        actionOnEndOfPath = a;
	}

    /// <summary>
    /// Cambia el color del player al color especificado durante el tiempo especificado
    /// </summary>
    /// <param name="dmgColor">Color al que cambia</param>
    /// <param name="duration">Tiempo que dura la vuelta del color original</param>
    //IEnumerator DamageEffectSequence(Color dmgColor, float duration) {
    //	SpriteRenderer sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
    //	sr.color = dmgColor;
    //	for (float t = 0; t < 1.0f; t += Time.deltaTime / duration) {
    //		sr.color = Color.Lerp(dmgColor, originalColor, t);
    //		yield return null;
    //	}
    //	sr.color = originalColor;
    //}

    public override void Die(HittableMulti lastHitter = null) {
        if(lastHitter.GetType() == typeof(PenguinResourcesMulti))
            GameManager.instance.OnMinionDie(imBlue);       
        Destroy(this.gameObject);
    }

    public void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position+new Vector3(0,.25f,0), rangeOfAtack);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(target, distanceToGoToTheNextWayPoint);
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, distanceToGoToAttack);

	}
    
    public override void SetDebuff(ElementButton type ,byte skillSelected){
        if(type == ElementButton.None) return;
        if(!actuallyBuffedOrDebuffed) {
            //print("Aplico debuff en " + transform.name);
            actuallyBuffedOrDebuffed = true;
            SOHabilidad act = GameManager.instance.hud.GetSkillByByte(skillSelected);
            StartCoroutine(PonerDebuffos(type, act));
        }
    }

    Coroutine onFireCor;
    IEnumerator PonerDebuffos(ElementButton type, SOHabilidad soBuffs) {
        float baseSpeed = agent.speed;
        if(type == ElementButton.Earth) 
            agent.speed *= soBuffs.slow;
       
        if(type == ElementButton.Fire) 
            onFireCor = StartCoroutine(OnFire(soBuffs.quemadura));
        

        yield return new WaitForSeconds(soBuffs.buffent);

        if(type == ElementButton.Earth) 
            agent.speed = baseSpeed;
       
        if(type == ElementButton.Fire) 
            StopCoroutine(onFireCor);
        

        actuallyBuffedOrDebuffed = false;
    }

    IEnumerator OnFire(float dmg) {
        while(true) {
            yield return new WaitForSeconds(.25f);
            Hit(dmg, quienMePega, true);
        }
    }


}
