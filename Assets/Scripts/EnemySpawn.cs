﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class EnemySpawn : NetworkBehaviour{

       
    public Oleada[] oleadas;
    public float primerSpawn = 1f;
    //public float tiempoEntreRondas = 5f;
    public float enemiesSpeed = 1; 
    public int oleadaActual = 0;
	public Transform minionParent;
 
    /// <summary>
    /// Esta clase contiene cada una de las oleadas con la lista de enemigos a instanciar,
    /// la lista de puntos para su path y cada cuanto deben aparecer
    /// </summary>
    [System.Serializable]
    public class Oleada {
        public float timeBetweenEnemies = 0.5f;
        [HideInInspector] public int enemiesInstantiated = 0;
        [HideInInspector] public int enemiesDead = 0;
        public Waypoints waypoints;
        public GameObject[] enemies;
    }
    private void Start(){}

    public void Spawn(bool blue) {
        if(isServer)
            SpawnEnemies(blue);
	}

    [Server]
    public void SpawnEnemies(bool blue) {
		StartCoroutine(SpawnEnemy(blue));
    }

    /// <summary>
    /// Spawnea un enemigo y en caso de que haya más se repite a si misma hasta que no hay más
    /// </summary>
    IEnumerator SpawnEnemy(bool blue) {
		Oleada o = oleadas[oleadaActual]; // Por Comodidad

        if (o.waypoints == null) yield return null; // Si no hay puntos que pare el spawn

        // Esperar el tiempo entre enemigos
        yield return new WaitForSeconds(o.timeBetweenEnemies);
        if(o.enemies == null) yield return null;

		// Genera el enemigo que necesita y le pone sus datos
        if (o.enemies[o.enemiesInstantiated] != null) {
            Enemy auxEn = Instantiate(o.enemies[o.enemiesInstantiated], o.waypoints.transform.parent).GetComponent<Enemy>();
			auxEn.transform.SetParent(blue ? minionParent.GetChild(0) : minionParent.GetChild(1));
			auxEn.name += " " + o.enemiesInstantiated;
            auxEn.transform.position = o.waypoints.points[0];
            auxEn.whoSpawnedMe = this;
            auxEn.target = o.waypoints.points[0];
            auxEn.roundWhereIsSpawned = oleadaActual;
            auxEn.imBlue = blue;
            NetworkServer.Spawn(auxEn.gameObject);
            auxEn.SpawnEnemy();   
			auxEn.ChangeOutlineColor(blue);
        }

        //Si hay más enemigos llama repite la corrutina
        o.enemiesInstantiated++;
        if (o.enemiesInstantiated < o.enemies.Length)
            StartCoroutine(SpawnEnemy(blue));
		else
			oleadas[oleadaActual].enemiesInstantiated = 0;
	}
     
}

