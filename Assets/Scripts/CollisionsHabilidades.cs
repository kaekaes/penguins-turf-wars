 using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CollisionsHabilidades : NetworkBehaviour {
	[System.Serializable]
	public struct HitPoint {
		public Transform point1;
		public float range1;
		public Transform point2;
		public float range2;
		public Transform point3;
		public float range3;

	}

	[Header("Collisions")]
	public HitPoint hitPoints;
	private LayerMask playerLayer;

	[HideInInspector] public Transform myPlayer;
	private PenguinControllerMulti myPlayerController;

	[Header("Particuals")]
	public ParticleSystem ps;
	[HideInInspector] public SOHabilidad skill;
	   
	public void Start() {
		playerLayer = LayerMask.GetMask("Clickable");

		myPlayer.TryGetComponent<PenguinControllerMulti>(out myPlayerController);
	}

	public void StartParticulas() {
		if (ps != null)
			ps.Play();
	}

	public void OnlyStopParticles() {
		if (ps != null)
			ps.Stop();
	}

	public void OnlyDestroy() {
		Destroy(transform.parent.gameObject);
	}

	public void OnlyEndAnimation() {
		OnlyDestroy();
	}

	public void StopParticulas() {
		OnlyStopParticles();
		OnlyEndAnimation();
	}

	public void CheckCollisions() {
		if (!isServer)
			return;
		if (hitPoints.range1 > 0)
			CheckColision(hitPoints.point1.position, hitPoints.range1);

		if (hitPoints.range2 > 0)
			CheckColision(hitPoints.point2.position, hitPoints.range2);

		if (hitPoints.range3 > 0)
			CheckColision(hitPoints.point3.position, hitPoints.range3);
	}

	[Server]
	private void CheckColision(Vector3 pos, float range) {
		//print("Checking colls");
		Collider[] hits = Physics.OverlapSphere(pos, range, playerLayer);
		foreach (Collider hit in hits) {
			//print("Hit something");
			if (hit.transform == myPlayer)
				continue;

			hit.TryGetComponent<HittableMulti>(out HittableMulti hitted);
			if (hitted != null) {
				if (hit.transform.TryGetComponent<Enemy>(out Enemy hitEn))
					if (hitEn.imBlue == myPlayerController.imBlue)
						continue;


				if (hit.transform.TryGetComponent<PenguinResourcesMulti>(out PenguinResourcesMulti hitPen))
					if (hitPen.manager.imBlue == myPlayerController.imBlue)
						continue;


                if(hit.transform.TryGetComponent<NexusController>(out NexusController hitNe))
                    if(hitNe.imBlue == myPlayerController.imBlue)
                        continue;


                if(hit.transform.TryGetComponent<Tower>(out Tower hitT))
                    if(hitT.imBlue == myPlayerController.imBlue)
                        continue;


                hitted.Hit(skill.damageHabilidad * (skill.nivelHabilidad * myPlayerController.DMGBase), myPlayerController.penguinResources);
                byte byteHabilidad = GameManager.instance.hud.GetByteOfSkill(skill);

                hitted.SetDebuff(skill.element1, byteHabilidad);
                hitted.SetDebuff(skill.element2, byteHabilidad);
            }
        }
	}

	public void OnDrawGizmos() {
		if (hitPoints.point1 != null) {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(hitPoints.point1.position, hitPoints.range1);
		}

		if (hitPoints.point2 != null) {
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere(hitPoints.point2.position, hitPoints.range2);
		}
		if (hitPoints.point3 != null) {
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireSphere(hitPoints.point3.position, hitPoints.range3);
		}
	}
}


