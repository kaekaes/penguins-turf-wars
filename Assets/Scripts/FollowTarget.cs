/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using UnityEngine;

public class FollowTarget : MonoBehaviour{

	public Transform target;
	public float smoothSpeed = 0.125f;
	public Vector3 offset;

	void LateUpdate() {
        if(target != null)
		    transform.position = target.position+offset;

	}
}
