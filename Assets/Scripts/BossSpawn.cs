﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

/// <summary>
/// Actua como spawner de los enemigos de la jungla
/// Aunque se llama BossSpawn debería de ser JungleEnemySpawner
/// </summary>
public class BossSpawn : NetworkBehaviour{

    public GameObject boss;
	private bool blue;
	public float timeToRespawn = 30f;

    private void Start(){}

	/// <summary>
	/// Manda a aparecer los diferentes enemigos
	/// </summary>
	/// <param name="blue"></param>
    public void Spawn(bool blue){
		this.blue = blue;
        if(isServer) SpawnEnemies(transform);
	}

    [Server]
    public void SpawnEnemies(Transform parent) {
		StartCoroutine(SpawnEnemy(parent));
    }

    /// <summary>
    /// Spawnea un enemigo y en caso de que haya más se repite a si misma hasta que no hay más
    /// </summary>
    IEnumerator SpawnEnemy(Transform parent, float time = 1f) {
        // Esperar el tiempo entre enemigos
        yield return new WaitForSeconds(time);
        if(boss == null) yield return null;

		// Genera el enemigo que necesita y le pone sus datos
		else { 
            Boss auxEn = Instantiate(boss).GetComponent<Boss>();
            auxEn.transform.position = parent.position;
			auxEn.spawnPos = parent.position;
			auxEn.whoSpawnedMee = this;
			NetworkServer.Spawn(auxEn.gameObject);
            auxEn.SpawnEnemy();   
        }
	}

	public void Respawn() {
		SpawnEnemies(transform);

	}

}

