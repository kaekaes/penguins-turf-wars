using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProfileManager : MonoBehaviour {

	public SOPlayer _SESSION;
	public TextMeshProUGUI txtUsername;
	public TextMeshProUGUI txtLvl;
	public Slider slidExp;
	public SOControles controles;

	public Transform[] contrKeys, contrVals;

	public List<SOHabilidad> habilidades = new List<SOHabilidad>();
	public Material staffMat;
	public Light staffLight;

	void Start() {
        if(_SESSION != null) {
            txtLvl.text = _SESSION.lvl + "";
            txtUsername.text = _SESSION.username;
            slidExp.value = (float)_SESSION.exp / 100f * (Mathf.Pow(2, _SESSION.lvl - 1) * 0.75f);
        

			if(_SESSION.id == 0)
				SceneUtils.LastScene();
		}

		SceneUtils.Instance.saveCoroutine = StartCoroutine(GetComponent<SceneUtils>().GetControls(_SESSION));




	}

	public void PrepareKeys() {
        print("Poniendo las keys como toca");
		int i = 0;
		foreach (KeyValuePair<ElementButton, KeyCode> val in controles.keys) {
			contrKeys[i].name = "txt" + val.Key.ToString();
			contrKeys[i].GetChild(0).GetComponent<TextMeshProUGUI>().text = val.Key.ToString();
			contrVals[i].name = val.Key.ToString();
			contrVals[i].GetChild(0).GetComponent<TextMeshProUGUI>().text = val.Value.ToString();
			i++;
		}
	}


	public void SetColor(string elem) {
		SOHabilidad aux = habilidades.Find(name => name.Name == elem);
		if (aux != null) {
			if (staffMat != null)
				staffMat.color = aux.staffColor;
			if (staffLight != null)
				staffLight.color = aux.staffColor;
		}
	}

	public void UnsetColor() {
		if(staffMat != null)
			staffMat.color = Color.white;
		if(staffLight != null)
			staffLight.color = Color.white;
			
	}

}
