using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinObject : MonoBehaviour {

	private Vector3 startSize;

	public Vector3 spinSpeed;
	public float growSpeed;
	public float offsetSize = 0.5f;

	
	void Start() {
		startSize = transform.localScale;
	}

	void Update() {
		transform.localScale = (startSize * Mathf.Abs(Mathf.Sin(Time.time * growSpeed) / 4 + 0.5f)) + (startSize * offsetSize);
		transform.Rotate(spinSpeed);
	}
}
