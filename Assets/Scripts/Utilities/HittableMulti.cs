using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class HittableMulti : NetworkBehaviour{

	[SyncVar (hook = nameof(HookMaxHP))]
	public float maxHp;

	[SyncVar (hook = nameof(HookHP))]
	[ShowInInspector] protected float hp;

	[SyncVar(hook = nameof(HookColor))]
	protected Color color;

	[SyncVar]
	private bool invencible;
	private float invencibleTime = 0.25f;
    public bool actuallyBuffedOrDebuffed;

	
	protected virtual void Awake() {
		ResetHP();
	}

	public virtual void ResetHP() => hp = maxHp;

	public bool Alive() {
		return hp > 0;
	}

    [Server]
	public virtual bool Hit(float dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
        if (!invencible || skipInvulnerable && GameManager.instance.gameStarted) {
			hp -= dmg;

            if(hp <= 0)
                Die(lastHitter);

            if(!skipInvulnerable) {
                invencible = true;
                Invoke(nameof(Desinvencibilice), invencibleTime);
            }

            //print(transform.name + " got hit. " + hp + " / " + maxHp);
        }
		return Alive();
	}

    [Server]
    public virtual void Hit(double dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
		if (!invencible || skipInvulnerable && GameManager.instance.gameStarted) {
			hp -= (float) dmg;

            if(hp <= 0)
                Die(lastHitter);

            if(!skipInvulnerable) {
                invencible = true;
			    Invoke(nameof(Desinvencibilice), invencibleTime);
            }
            //print(transform.name + " got hit. " + hp + " / " + maxHp);
        }
	}

	private void Desinvencibilice() {
		invencible = false;
	}


	public virtual void HookMaxHP(float inutil, float actHp) {
		maxHp = actHp;
	}

	public virtual void HookHP(float inutil, float actHp) {
		hp = actHp;
	}

	public virtual void HookColor(Color inutil, Color actColor) {
		color = actColor;
        if(transform.childCount > 0) {
            transform.GetChild(0).TryGetComponent<MeshRenderer>(out MeshRenderer mr);
            if(mr != null)
                mr.material.color = actColor;
        } else {
            transform.TryGetComponent<MeshRenderer>(out MeshRenderer mr);
            if(mr != null)
                mr.material.color = actColor;
        }
    }

    public virtual void Die(HittableMulti lastHitter) {}

    public virtual void SetDebuff(ElementButton type, byte skillSelected) {}
	protected void DamageEffectSequence(Color dmgColor, float duration) {
        if(GameManager.instance.gameStarted)
		    StartCoroutine(DamageEffectSequenceC(dmgColor, duration));
	}

	protected IEnumerator DamageEffectSequenceC(Color dmgColor, float duration) {
		color = dmgColor;

		for (float t = 0; t < 1.0f; t += Time.deltaTime / duration) {
			color = Color.Lerp(dmgColor, Color.white, t);
			yield return null;
		}

		color = Color.white;
	}

	protected IEnumerator DamageEffectSequenceOnServer(MeshRenderer mr, Color dmgColor, float duration) {
		mr.material.color = dmgColor;

		for (float t = 0; t < 1.0f; t += Time.deltaTime / duration) {
			mr.material.color = Color.Lerp(dmgColor, Color.white, t);
			yield return null;
		}

		mr.material.color = Color.white;
	}
}
