public static class Skills{//       AEWF
    public const byte FIRE		= 0b0001;//1
    public const byte WATER		= 0b0010;//2
    public const byte EARTH		= 0b0100;//4
    public const byte AIR		= 0b1000;//8
    public const byte STEAM		= 0b0011;//3
    public const byte LAVA		= 0b0101;//5
    public const byte BURN		= 0b1001;//9
    public const byte WOOD		= 0b0110;//6
    public const byte ICE		= 0b1010;//10
    public const byte EXPLOSION = 0b1100;//12

}
