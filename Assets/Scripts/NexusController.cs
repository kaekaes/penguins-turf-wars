﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NexusController : HittableMulti{

	private Transform crystal;    
    public float blood;
	public bool imBlue;

	public float barrierMinSize = 30;
	[Range(30, 320)]
	public float barrierSize = 30;
    public float BarrierSize { get { return barrierSize; }
        set {
            barrierSize = value;
            StartCoroutine(ScaleBarrier());
        }
    }
    private int maxRadiusBarrier;

	void Start() {
		crystal = transform.parent.parent.GetChild(0);
		maxRadiusBarrier = GameManager.instance.maxTotalBarrier;
		BarrierSize = barrierMinSize;
	}

	IEnumerator ScaleBarrier() {
		Vector3 act = crystal.localScale;
		for (float t = 0; t < 1f; t += Time.deltaTime) {
			crystal.localScale = Vector3.Lerp(act,new Vector3(barrierSize, barrierSize, barrierSize), t);
			yield return null;
		}
	}

    public override bool Hit(float dmg, HittableMulti lastHitter, bool skipInvulnerable = false) {
        base.Hit((double)dmg, lastHitter, skipInvulnerable);

        Color hitColor = Color.red * Mathf.Pow(2, 1.2f);
        DamageEffectSequence(hitColor, .5f);
        StartCoroutine(DamageEffectSequenceOnServer(transform.GetComponent<MeshRenderer>(), hitColor, .5f));
        return Alive();
    }

    public void AddBlood(float puntos) {
		NexusController other = GameManager.instance.GetNexusController(!imBlue);

		BarrierSize = barrierSize + puntos <= (maxRadiusBarrier - barrierMinSize) ? barrierSize + puntos : maxRadiusBarrier - barrierMinSize;

		float sumaBarreras = barrierSize + other.barrierSize;

		if (sumaBarreras >= maxRadiusBarrier && barrierSize <= maxRadiusBarrier - barrierMinSize) {
			//Si no se hace así 2 veces no se actualiza bien y a veces una barrera supera la otra
			other.barrierSize -= sumaBarreras - maxRadiusBarrier; 
			other.BarrierSize = (other.barrierSize * maxRadiusBarrier) / sumaBarreras;
		}
	}

    public override void Die(HittableMulti lastHitter) {
        print("He murido " + transform.name);
        print("La partida debería acabar pero está WIP");
        GameManager.instance.hud.FinPartida(imBlue);
    }
}
