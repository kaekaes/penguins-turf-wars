using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

/// <summary>
/// Clase auxiliar de GameManager para hacer la pantalla de carga porque requiere ser NetworkBehaviour
/// </summary>
public class FindGame : NetworkBehaviour{

	/// <summary>
	/// Slo se puede llamar desde servidor.
	/// Empieza la partida para el servidor y tambin la empieza con un ClientRPC para que los dems clientes tambin lo hagan
	/// </summary>
	/// <param name="timeToStartTheGame"></param>
	[Server]
	public void StartGame(float timeToStartTheGame) {
		StartCoroutine(StartGameC(timeToStartTheGame));
		StartGameRPC(timeToStartTheGame);
	}

	/// <summary>
	/// Esta nicamente empieza la carga en los clientes
	/// </summary>
	/// <param name="timeToStartTheGame"></param>
	[ClientRpc]
	private void StartGameRPC(float timeToStartTheGame) => StartCoroutine(StartGameC(timeToStartTheGame));

	/// <summary>
	/// Muestra a todos los jugadores el texto de que se ha encontrado oponente y empieza un contador
	/// Al acabar empieza la partida
	/// </summary>
	/// <param name="timeToStartTheGame"></param>
	/// <returns></returns>
	IEnumerator StartGameC(float timeToStartTheGame) {
		GameManager.instance.hud.SetLoadingScreenMessage("Opponent found");
		for (int i = 0; i < 2; i++) {
			yield return new WaitForSeconds(1);
		}

		GameManager.instance.startSpawning = true;
		for (int i = 0; i < timeToStartTheGame; i++) {
			GameManager.instance.hud.SetLoadingScreenMessage("Starting game in " + (timeToStartTheGame - i));
			yield return new WaitForSeconds(1);

		}

		GameManager.instance.hud.ShowLoadingScreen(false);

		GameManager.instance.gameStarted = true;
		GameManager.instance.StartSpawningEnemies();
	}

}
