using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase encargada de gestionar el servidor, decide quien se une y como
/// </summary>
public class ManagerNetwork : NetworkManager {

    public Transform blueNexus, redNexus;
    public PenguinControllerMulti bluePen, redPen;
	public bool startGame;
	public float timeToStartTheGame = 10;
	public int minPlayers = 2;

    public override void OnServerAddPlayer(NetworkConnection conn) {
		GameManager.instance.server = this;

		Transform start = blueNexus;
        if(bluePen != null) 
            start = redNexus;
        

        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        player.TryGetComponent<PenguinControllerMulti>(out PenguinControllerMulti p);

		// El transform.name nicamente se cambia en el servidor pero nos viene bien para hacer debugging
        if (numPlayers == 1) {
			player.transform.name = "Jugador rojo";
			p.imBlue = false;
			redPen = p;
		} else {
			player.transform.name = "Jugador azul";
			p.imBlue = true;
			bluePen = p;
		}

		// Tras conectar al cliente le muestra la ventana de esperando por oponente
        NetworkServer.AddPlayerForConnection(conn, player);
		GameManager.instance.hud.ShowLoadingScreen();
		GameManager.instance.hud.SetLoadingScreenMessage("Waiting for your Opponent");

		// En caso de ser 2 empieza el juego
		if (numPlayers == minPlayers && !startGame) {
			if (bluePen == null || redPen == null)
				FullFillGamePlayers();
			GameManager.instance.StartGame(timeToStartTheGame);
		}
	}
    
	/// <summary>
	/// Mtodo que te permite obtener a un jugador dependiendo del bool
	/// </summary>
	/// <param name="blue"></param>
	/// <returns>Devuelve el Manager del jugador</returns>
    public PenguinControllerMulti GetPlayer(bool blue) {
		if (bluePen == null || redPen == null)
			FullFillGamePlayers();
		return blue ? bluePen : redPen;
	}

	/// <summary>
	/// Rellena de forma sucia la lista de los clientes
	/// Esta funcin slo se llama como ltimo recurso en caso de que ocurra algn error
	/// </summary>
	public void FullFillGamePlayers() {
		PenguinControllerMulti[] a = FindObjectsOfType<PenguinControllerMulti>();
		if (a.Length == 1)
			bluePen = a[0];
		else if(a.Length == 2) {
			if (a[0].imBlue) {
				bluePen = a[0];
				redPen = a[1];
			} else {
				bluePen = a[1];
				redPen = a[0];
			}
		}
	}


	public override void OnClientDisconnect(NetworkConnection conn) {
		GameManager.instance.gameStarted = false;
		GameManager.instance.server.bluePen?.PlayerLeave();
		GameManager.instance.server.redPen?.PlayerLeave();
		base.OnClientDisconnect(conn);
	}

}
