using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SharedLibrary.Responses;
using UnityEngine;
using UnityEngine.Networking;

namespace Atherox.BackEnd {
    public class HttpClient {
        public static async Task<T> Get<T>(string endpoint, string bearerKey = null) {
            var getReq = CreateReq(endpoint, bearerKey: bearerKey);
            getReq.SendWebRequest();

            while (!getReq.isDone) await Task.Delay(10);
            if (getReq.responseCode == 0)
                throw new EndpointException(500, "Server unreachable, Retry in a minute");
            if (getReq.responseCode != 200)
                throw new EndpointException(getReq.responseCode, getReq.downloadHandler.text);

            return JsonConvert.DeserializeObject<T>(getReq.downloadHandler.text);
        }

        public static async Task<T> Post<T>(string endpoint, object payload, string bearerKey = null) {
            var getReq = CreateReq(endpoint, RequestType.POST, payload, bearerKey);
            getReq.SendWebRequest();

            while (!getReq.isDone) await Task.Delay(10);
            if (getReq.responseCode == 0)
                throw new EndpointException(500, "Server unreachable, Retry in a minute");
            if (getReq.responseCode != 200)
                throw new EndpointException(getReq.responseCode, getReq.downloadHandler.text);
            return JsonConvert.DeserializeObject<T>(getReq.downloadHandler.text);
        }

        private static void AttachHeader(UnityWebRequest req, string key, string val) => req.SetRequestHeader(key, val);

        private static UnityWebRequest CreateReq(string path, RequestType type = RequestType.GET, object data = null,
            string bearerKey = null) {
            // ReSharper disable once HeapView.BoxingAllocation
            var req = new UnityWebRequest(path, type.ToString());

            if (data != null) {
                var jsonData = JsonConvert.SerializeObject(data);

                var bodyRaw = Encoding.UTF8.GetBytes(jsonData);
                req.uploadHandler = new UploadHandlerRaw(bodyRaw);
            }

            req.downloadHandler = new DownloadHandlerBuffer();
            req.SetRequestHeader("accept", "*/*");
            req.SetRequestHeader("Content-Type", "application/json");

            if (bearerKey != null)
                AttachHeader(req, "Authorization", $"Bearer {bearerKey}");

            return req;
        }
    }

    public enum RequestType {
        GET = 0,
        POST,
        PUT
    }

    [Serializable]
    public class EndpointException : Exception {
        public EndpointException() {
        }

        public EndpointException(long numErr, String msg) : base(
            String.Format("Error {0}: {1}", numErr.ToString(), msg)) {
        }
    }
}